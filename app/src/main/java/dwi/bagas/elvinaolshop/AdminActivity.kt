package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity: AppCompatActivity(),View.OnClickListener {

    var id_peg = ""
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btnKelolaPegawai ->{
                val intent = Intent(this,PegawaiActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKelolaBarang ->{
                val intent = Intent(this,BarangActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKelolaKategori ->{
                val intent = Intent(this,KategoriActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKelolaTrx ->{
                val intent = Intent(this,TransaksiActivity::class.java)
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()

        btnKelolaPegawai.setOnClickListener(this)
        btnKelolaBarang.setOnClickListener(this)
        btnKelolaKategori.setOnClickListener(this)
        btnKelolaTrx.setOnClickListener(this)

        btnLogoff.setOnClickListener {
            Toast.makeText(this,"Berhasil Logout", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}