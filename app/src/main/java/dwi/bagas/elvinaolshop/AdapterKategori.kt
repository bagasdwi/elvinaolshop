package dwi.bagas.elvinaolshop

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterKategori(val dataKat: List<HashMap<String,String>>,
                          val kategoriActivity: KategoriActivity) : //new
    RecyclerView.Adapter<AdapterKategori.HolderDataKategori>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterKategori.HolderDataKategori {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kategori,p0,false)
        return HolderDataKategori(v)
    }

    override fun getItemCount(): Int {
        return dataKat.size
    }

    override fun onBindViewHolder(p0: AdapterKategori.HolderDataKategori, p1: Int) {
        val data = dataKat.get(p1)
        p0.txIdKategori.setText(data.get("id_kat"))
        p0.txNamaKategori.setText(data.get("nm_kat"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            var idk = data.get("id_kat").toString()
            kategoriActivity.showDetail(idk)
        })
        //endNew
    }

    class HolderDataKategori(v: View) : RecyclerView.ViewHolder(v){
        val txIdKategori = v.findViewById<TextView>(R.id.txIdkategori)
        val txNamaKategori = v.findViewById<TextView>(R.id.txNamaKategori)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutKat)
    }
}