package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_tambah_detail_trx.*
import org.json.JSONArray

class TambahDBActivity : AppCompatActivity(), View.OnClickListener {

    val F_BKD = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_BGAMBAR = "url"
    val F_BSTOK = "stok"
    val F_KNAMA = "nm_kat"
    var url = "http://elvinatoko.000webhostapp.com/show_barang.php"
    var id_peg = ""
    var id_trx = ""
    var edt = ""

    lateinit var alBarang : ArrayList<HashMap<String,Any>>
    lateinit var adapterBarang: AdapterBarangLV

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnCariBrgTrx ->{
                showBarang(edCariBrgTrx.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_detail_trx)
        alBarang = ArrayList() //new
        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
        edt = paket?.getString("edit").toString()
        id_trx = paket?.getString("id_trx").toString()
        btnCariBrgTrx.setOnClickListener(this)
        lsBarang.onItemClickListener = itemClick
    }

    override fun onStart() {
        super.onStart()
        showBarang("")
    }

    fun showBarang(brg: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                alBarang.clear()
                Log.d("isi","$response")
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    val hm = HashMap<String,Any>()
                    hm.put(F_BKD,jsonObject.getString(F_BKD))
                    hm.put(F_BNAMA,jsonObject.getString(F_BNAMA))
                    hm.put(F_BSTOK,jsonObject.getString(F_BSTOK))
                    hm.put(F_KNAMA,jsonObject.getString(F_KNAMA))
                    hm.put(F_BGAMBAR,jsonObject.getString(F_BGAMBAR))
                    alBarang.add(hm)
                }
                adapterBarang = AdapterBarangLV(this,alBarang)
                lsBarang.adapter = adapterBarang
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_barang",brg)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alBarang.get(position)
        var kdb = hm.get(F_BKD).toString()
        val intent = Intent(this, DetailBTrxActivity::class.java  )
        intent.putExtra("kd_barang",kdb)
        intent.putExtra("id_peg",id_peg)
        if(edt=="edit"){
            intent.putExtra("edit","edit")
            intent.putExtra("id_trx",id_trx)
        }
        startActivity(intent)
        finish()
    }

}