package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_barang_trx.*
import org.json.JSONObject


class DetailBTrxActivity: AppCompatActivity(),View.OnClickListener{

    var kdb = ""
    var id_peg = ""
    var id_trx = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_barang.php"
    var edt = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnToCart ->{
                var intent = Intent(this, TambahTransaksiActivity::class.java  )
                if(edt=="edit"){
                    intent = Intent(this, EditTrxActivity::class.java  )
                    intent.putExtra("id_trx",id_trx)
                }
                intent.putExtra("kd_barang",kdb)
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_barang_trx)

        var paket: Bundle? = intent.extras
        kdb = paket?.getString("kd_barang").toString()
        id_peg = paket?.getString("id_peg").toString()
        id_trx = paket?.getString("id_trx").toString()
        edt = paket?.getString("edit").toString()
        btnToCart.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDetailBarang()
    }

    fun showDetailBarang() {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                txKdDB.setText(kdb)
                txNamaDB.setText(jsonObject.getString("nm_barang"))
                txKatDB.setText(jsonObject.getString("nm_kat"))
                Picasso.get().load(jsonObject.getString("url")).into(imGambarDB);
                txStokDB.setText(jsonObject.getString("stok"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("kd_barang",kdb)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}