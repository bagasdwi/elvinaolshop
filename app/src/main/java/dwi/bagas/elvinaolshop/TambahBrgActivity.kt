package dwi.bagas.elvinaolshop

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_tambah_barang.*
import org.json.JSONArray
import org.json.JSONObject

class TambahBrgActivity: AppCompatActivity(), View.OnClickListener{

    lateinit var dialog : AlertDialog.Builder
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var url = "http://elvinatoko.000webhostapp.com/query_crud_barang.php"
    var url2 = "http://elvinatoko.000webhostapp.com/show_kategori.php"
    var imStr = ""
    var nmFile = ""
    var pilihKategori = ""
    var fileUri = Uri.parse("")
    lateinit var mediaHelper: MediaHelper
    var daftarKategori = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_barang)

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKategori)
        spKategori.adapter = kategoriAdapter
        spKategori.onItemSelectedListener = itemSelected

        mediaHelper = MediaHelper(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch (e: Exception){
            e.printStackTrace()
        }

        dialog = AlertDialog.Builder(this)
        btnTambahBrgT.setOnClickListener(this)
        imGambarBrgT.setOnClickListener(this)
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }
    }

    fun getNamaKategori(namaKategori : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                Log.d("isi","$response")
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nm_kat"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nm_kat",namaKategori)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode==mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imGambarBrgT,fileUri)
                nmFile = mediaHelper.getMyFileName()
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahBrgT->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.imGambarBrgT ->{
                requestPermissions()
            }
        }
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        tambah()
    }

    fun tambah(){
        val request = object : StringRequest(
            Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    this.finish()
                }else if(error.equals("333")){
                    Toast.makeText(this,"Username telah digunakan", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","insert")
                hm.put("nm_barang",edNmBrgT.text.toString())
                hm.put("stok",edStokT.text.toString())
                hm.put("image",imStr)
                hm.put("file",nmFile)
                hm.put("nm_kat",pilihKategori)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }
    override fun onStart() {
        super.onStart()
        getNamaKategori("")
    }
}