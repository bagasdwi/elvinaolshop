package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_pegawai.*

class HalamanPegawaiActivity: AppCompatActivity(),View.OnClickListener {

    var id_peg = ""
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btnLihatBarangPeg ->{
                val intent = Intent(this,LihatBarangActivity::class.java)
                startActivity(intent)
            }
            R.id.btnLihatKategoriPeg ->{
                val intent = Intent(this,LihatKategoriActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKelolaTrxPeg ->{
                val intent = Intent(this,TransaksiKasirActivity::class.java)
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pegawai)

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()

        btnLihatBarangPeg.setOnClickListener(this)
        btnLihatKategoriPeg.setOnClickListener(this)
        btnKelolaTrxPeg.setOnClickListener(this)

        btnLogoffPeg.setOnClickListener {
            Toast.makeText(this,"Berhasil Logout", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}