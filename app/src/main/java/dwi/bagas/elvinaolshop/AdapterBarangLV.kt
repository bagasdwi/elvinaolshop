package dwi.bagas.elvinaolshop

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class AdapterBarangLV(val context: Context,
                      arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter(){

    val F_BKD = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_BGAMBAR = "url"
    val F_BSTOK = "stok"
    val F_KNAMA = "nm_kat"
    var uri = Uri.EMPTY
    val list = arrayList

    inner class ViewHolder(){
        var txKdBrg : TextView? = null
        var txNmBrg : TextView? = null
        var txKategori : TextView? = null
        var txStok : TextView? = null
        var imgBrg : ImageView? = null
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_barang, null, true)

            holder.txKdBrg = view!!.findViewById(R.id.txKdBrg) as TextView
            holder.txNmBrg = view!!.findViewById(R.id.txNmBrg) as TextView
            holder.txKategori = view!!.findViewById(R.id.txKategori) as TextView
            holder.txStok = view!!.findViewById(R.id.txStok) as TextView
            holder.imgBrg = view!!.findViewById(R.id.imgBrg) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        holder.txKdBrg!!.setText(list.get(position).get(F_BKD).toString())
        holder.txNmBrg!!.setText(list.get(position).get(F_BNAMA).toString())
        holder.txKategori!!.setText(list.get(position).get(F_KNAMA).toString())
        holder.txStok!!.setText(list.get(position).get(F_BSTOK).toString())

        uri = Uri.parse(list.get(position).get(F_BGAMBAR).toString())
        if (!uri.equals("")){Picasso.get().load(uri).into(holder.imgBrg)}
        return view!!
    }
}
