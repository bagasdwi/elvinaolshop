package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kelola_pegawai.*
import kotlinx.android.synthetic.main.activity_kelola_pegawai.btnTambahPeg
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tambah_kategori.*
import kotlinx.android.synthetic.main.activity_tambah_pegawai.*
import org.json.JSONObject

class TambahPegawaiActivity: AppCompatActivity(), View.OnClickListener{
    lateinit var dialog : AlertDialog.Builder
    var url = "http://elvinatoko.000webhostapp.com/query_crud_pegawai.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_pegawai)

        dialog = AlertDialog.Builder(this)
        btnTambahPeg.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahPeg->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        tambah()
    }

    fun tambah(){
        val request = object : StringRequest(
            Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Berhasil menambah data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else if(error.equals("333")){
                    Toast.makeText(this,"Username telah digunakan", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Gagal menambah data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","insert")
                hm.put("nm_peg",edNmPegT.text.toString())
                hm.put("username",edUsernameT.text.toString())
                hm.put("password",edPassT.text.toString())

                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}