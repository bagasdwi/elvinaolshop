package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_konfirm_tambah_trx.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat

class KonfirmTambahTrxActivity: AppCompatActivity(), View.OnClickListener {

    val F_TID = "id_trx"
    val F_BID = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_DTHARGA = "harga"
    val F_DTJML = "jumlah"

    lateinit var alTrx : ArrayList<HashMap<String,Any>>
    lateinit var adapterDetail: AdapterDetailTransaksi
    var url = "http://elvinatoko.000webhostapp.com/show_detail_transaksi.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_transaksi.php"
    var id_peg = ""
    var id_trx = ""
    var pilihTrx = ""
    var total = 0

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBatalTambah ->{
                val intent = Intent(this, TambahTransaksiActivity::class.java  )
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
            }
            R.id.btnProsesTambah ->{
                tambahData()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirm_tambah_trx)
        alTrx = ArrayList() //new

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
        id_trx = paket?.getString("id_trx").toString()
        btnBatalTambah.setOnClickListener(this)
        btnProsesTambah.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showData()
    }

    fun showData(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                alTrx.clear()
                Log.d("isi","$response")
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    val hm = HashMap<String,Any>()
                    hm.put(F_TID, jsonObject.getString(F_TID))
                    hm.put(F_BID, jsonObject.getString(F_BID))
                    hm.put(F_BNAMA, jsonObject.getString(F_BNAMA))
                    var harga = jsonObject.getString(F_DTHARGA).toString()
                    hm.put(F_DTHARGA, harga)
                    var jumlah = jsonObject.getString(F_DTJML).toString()
                    hm.put(F_DTJML, jumlah)
                    var kali = harga.toInt() * jumlah.toInt()
                    total += kali
                    alTrx.add(hm)
                }
                adapterDetail = AdapterDetailTransaksi(this,alTrx)
                lsDetailTrx.adapter = adapterDetail
                var totalrp = formatRp(total.toString())
                txTotalCart.setText("Rp $totalrp")
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_trx",id_trx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun formatRp(angka:String): String? {
        val dec = DecimalFormat("#,###.##")
        return dec.format(angka.toInt())
    }

    fun tambahData(){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Data penjualan berhasil ditambahkan!", Toast.LENGTH_LONG).show()
                    finish()
                }else if(error.equals("222")){
                    Toast.makeText(this, "Data penjualan ditambahkan, mohon tunggu verifikasi dari admin.", Toast.LENGTH_LONG).show()
                    finish()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","proses")
                hm.put("id_trx",id_trx)
                hm.put("id_peg",id_peg)
                hm.put("customer",edNamaCust.text.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}