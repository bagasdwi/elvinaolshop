package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_detail_trx.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat

class DetailTrxActivity: AppCompatActivity(), View.OnClickListener {

    val F_TID = "id_trx"
    val F_BID = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_DTHARGA = "harga"
    val F_DTJML = "jumlah"

    lateinit var alTrx : ArrayList<HashMap<String,Any>>
    lateinit var adapterDetail: AdapterDetailTransaksi
    var url = "http://elvinatoko.000webhostapp.com/show_detail_transaksi.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_transaksi.php"
    var url3 = "http://elvinatoko.000webhostapp.com/show_transaksi.php"
    var id_peg = ""
    var id_trx = ""
    var kasir = ""
    var pilihTrx = ""
    var total = 0

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnVerifikasi ->{
                verifikasi("verif")
            }
            R.id.btnEditTrx ->{
                val intent = Intent(this, EditTrxActivity::class.java  )
                intent.putExtra("id_peg",id_peg)
                intent.putExtra("id_trx",id_trx)
                startActivity(intent)
            }
            R.id.btnHapusTrx ->{
                verifikasi("hapus")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_trx)
        alTrx = ArrayList() //new

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
        id_trx = paket?.getString("id_trx").toString()
        kasir = paket?.getString("kasir").toString()
        btnVerifikasi.setOnClickListener(this)
        btnEditTrx.setOnClickListener(this)
        btnHapusTrx.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        showData()
        showDetail()
    }

    fun showData(){
        val request = object : StringRequest(
            Request.Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                txIDDT.setText(id_trx)
                txTglDT.setText(jsonObject.getString("tanggal"))
                txCustDT.setText(jsonObject.getString("customer"))
                txPegDT.setText(jsonObject.getString("nm_peg"))
                var status = jsonObject.getString("status")
                txStatusDT.setText(status)
                when (status) {
                    "verifikasi" -> {
                        btnVerifikasi.visibility = View.VISIBLE
                        btnHapusTrx.visibility = View.VISIBLE
                    }
                    "berhasil" -> {
                        btnVerifikasi.visibility = View.GONE
                        btnHapusTrx.visibility = View.VISIBLE
                    }
                    else -> {
                        btnVerifikasi.visibility = View.GONE
                        btnHapusTrx.visibility = View.GONE
                    }
                }
                if(kasir=="kasir"){
                    btnHapusTrx.visibility = View.GONE
                    btnVerifikasi.visibility = View.GONE
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_trx",id_trx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDetail(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                alTrx.clear()
                Log.d("isi","$response")
                if (response!="") {
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length() - 1)) {
                        val jsonObject = jsonArray.getJSONObject(x)
                        val hm = HashMap<String, Any>()
                        hm.put(F_TID, jsonObject.getString(F_TID))
                        hm.put(F_BID, jsonObject.getString(F_BID))
                        hm.put(F_BNAMA, jsonObject.getString(F_BNAMA))
                        var harga = jsonObject.getString(F_DTHARGA).toString()
                        hm.put(F_DTHARGA, harga)
                        var jumlah = jsonObject.getString(F_DTJML).toString()
                        hm.put(F_DTJML, jumlah)
                        var kali = harga.toInt() * jumlah.toInt()
                        total += kali
                        alTrx.add(hm)
                    }
                    adapterDetail = AdapterDetailTransaksi(this, alTrx)
                    lsDetailTrxDT.adapter = adapterDetail
                    var totalrp = formatRp(total.toString())
                    txTotalCart.setText("Rp $totalrp")
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_trx",id_trx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun formatRp(angka:String): String? {
        val dec = DecimalFormat("#,###.##")
        return dec.format(angka.toInt())
    }

    fun verifikasi(mode : String){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Data penjualan berhasil diverifikasi!", Toast.LENGTH_LONG).show()
                    finish()
                }else if(error.equals("222")){
                    Toast.makeText(this, "Data penjualan dihapus.", Toast.LENGTH_LONG).show()
                    finish()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode",mode)
                hm.put("id_trx",id_trx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}