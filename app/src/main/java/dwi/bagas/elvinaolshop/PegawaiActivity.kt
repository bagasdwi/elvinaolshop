package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kelola_pegawai.*
import org.json.JSONArray

class PegawaiActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var pegAdapter : AdapterPegawai
    var daftarPeg = mutableListOf<HashMap<String,String>>()
    var url = "http://elvinatoko.000webhostapp.com/show_pegawai.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahPeg ->{
                val intent = Intent(this, TambahPegawaiActivity::class.java  )
                startActivity(intent)
            }
            R.id.btnCariPeg ->{
                showPegawai(edSearchPeg.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola_pegawai)

        pegAdapter = AdapterPegawai(daftarPeg,this) //new
        lsPeg.layoutManager = LinearLayoutManager(this)
        lsPeg.adapter = pegAdapter

        btnTambahPeg.setOnClickListener(this)
        btnCariPeg.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showPegawai("")
    }

    fun showPegawai(nmP: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPeg.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var peg = HashMap<String,String>()
                    peg.put("id_peg",jsonObject.getString("id_peg"))
                    peg.put("nm_peg",jsonObject.getString("nm_peg"))
                    peg.put("username",jsonObject.getString("username"))
                    peg.put("password",jsonObject.getString("password"))
                    peg.put("level",jsonObject.getString("level"))
                    daftarPeg.add(peg)
                }
                pegAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_peg",nmP)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun showDetail(idp : String){
        val intent = Intent(this, DetailPegActivity::class.java  )
        intent.putExtra("id_peg",idp)
        startActivity(intent)
    }
}