package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_lihat_kategori.*
import org.json.JSONArray

class LihatKategoriActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var katAdapter : AdapterLihatKategori
    var daftarKategori = mutableListOf<HashMap<String,String>>()
    var url = "http://elvinatoko.000webhostapp.com/show_kategori.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnCariKatPeg ->{
                showKategori(edSearchKatPeg.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lihat_kategori)

        katAdapter = AdapterLihatKategori(daftarKategori,this) //new
        lsKatPeg.layoutManager = LinearLayoutManager(this)
        lsKatPeg.adapter = katAdapter

        btnCariKatPeg.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showKategori("")
    }

    fun showKategori(nmK: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kat = HashMap<String,String>()
                    kat.put("id_kat",jsonObject.getString("id_kat"))
                    kat.put("nm_kat",jsonObject.getString("nm_kat"))
                    daftarKategori.add(kat)
                }
                katAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_kat",nmK)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun showDetail(idk : String){
        val intent = Intent(this, DetailKatPegActivity::class.java  )
        intent.putExtra("id_kat",idk)
        startActivity(intent)
    }
}