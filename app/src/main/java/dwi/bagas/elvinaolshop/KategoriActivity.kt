package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kelola_kategori.*
import org.json.JSONArray

class KategoriActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var katAdapter : AdapterKategori
    var daftarKategori = mutableListOf<HashMap<String,String>>()
    var url = "http://elvinatoko.000webhostapp.com/show_kategori.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahKat ->{
                val intent = Intent(this, TambahKategoriActivity::class.java  )
                startActivity(intent)
            }
            R.id.btnCariKat ->{
                showKategori(edSearchKat.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola_kategori)

        katAdapter = AdapterKategori(daftarKategori,this) //new
        lsKat.layoutManager = LinearLayoutManager(this)
        lsKat.adapter = katAdapter

        btnTambahKat.setOnClickListener(this)
        btnCariKat.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showKategori("")
    }

    fun showKategori(nmK: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kat = HashMap<String,String>()
                    kat.put("id_kat",jsonObject.getString("id_kat"))
                    kat.put("nm_kat",jsonObject.getString("nm_kat"))
                    daftarKategori.add(kat)
                }
                katAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_kat",nmK)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun showDetail(idk : String){
        val intent = Intent(this, DetailKatActivity::class.java  )
        intent.putExtra("id_kat",idk)
        startActivity(intent)
    }
}