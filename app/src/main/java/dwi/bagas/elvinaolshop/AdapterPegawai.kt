package dwi.bagas.elvinaolshop

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterPegawai(val dataPeg: List<HashMap<String,String>>,
                      val pegawaiActivity: PegawaiActivity) : //new
    RecyclerView.Adapter<AdapterPegawai.HolderDataPegawai>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterPegawai.HolderDataPegawai {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_pegawai,p0,false)
        return HolderDataPegawai(v)
    }

    override fun getItemCount(): Int {
        return dataPeg.size
    }

    override fun onBindViewHolder(p0: AdapterPegawai.HolderDataPegawai, p1: Int) {
        val data = dataPeg.get(p1)
        p0.txIdPeg.setText(data.get("id_peg"))
        p0.txNamaPeg.setText(data.get("nm_peg"))
        p0.txUsername.setText(data.get("username"))
        p0.txPasswd.setText(data.get("password"))
        p0.txLevel.setText(data.get("level"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            var idp = data.get("id_peg").toString()
            pegawaiActivity.showDetail(idp)
        })
        //endNew
    }

    class HolderDataPegawai(v: View) : RecyclerView.ViewHolder(v){
        val txIdPeg = v.findViewById<TextView>(R.id.txIdPeg)
        val txNamaPeg = v.findViewById<TextView>(R.id.txNmPeg)
        val txUsername = v.findViewById<TextView>(R.id.txUsername)
        val txPasswd = v.findViewById<TextView>(R.id.txPass)
        val txLevel = v.findViewById<TextView>(R.id.txLevel)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutPeg)
    }
}