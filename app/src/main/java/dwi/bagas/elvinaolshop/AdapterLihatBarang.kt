package dwi.bagas.elvinaolshop

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterLihatBarang(val dataBarang: List<HashMap<String,String>>,
                    val lihatbarangActivity: LihatBarangActivity) : //new
    RecyclerView.Adapter<AdapterLihatBarang.HolderBarang>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterLihatBarang.HolderBarang {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_barang, p0, false)
        return HolderBarang(v)
    }

    override fun getItemCount(): Int {
        return dataBarang.size
    }

    override fun onBindViewHolder(p0: AdapterLihatBarang.HolderBarang, p1: Int) {
        val data = dataBarang.get(p1)
        p0.txKdBrg.setText(data.get("kd_barang"))
        p0.txNmBrg.setText(data.get("nm_barang"))
        p0.txStok.setText(data.get("stok"))
        p0.txKategori.setText(data.get("nm_kat"))

        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            var kd_barang = data.get("kd_barang").toString()
            lihatbarangActivity.showDetail(kd_barang)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderBarang(v: View) : RecyclerView.ViewHolder(v) {
        val photo = v.findViewById<ImageView>(R.id.imgBrg)
        val txKdBrg = v.findViewById<TextView>(R.id.txKdBrg)
        val txNmBrg = v.findViewById<TextView>(R.id.txNmBrg)
        val txStok = v.findViewById<TextView>(R.id.txStok)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutBrg) //new
    }
}
