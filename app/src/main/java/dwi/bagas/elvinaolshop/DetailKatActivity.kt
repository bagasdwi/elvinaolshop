package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.acitivity_detail_kategori_admin.*
import org.json.JSONObject

class DetailKatActivity: AppCompatActivity(), View.OnClickListener {

    //Inisialisasi Variabel
    lateinit var dialog: AlertDialog.Builder
    var id_kat: String = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_kategori.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_kategori.php"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnEditKat -> {
                val intent = Intent(this, EditKatActivity::class.java  )
                intent.putExtra("id_kat",id_kat)
                startActivity(intent)
            }
            R.id.btnHapusKat -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acitivity_detail_kategori_admin)

        btnEditKat.setOnClickListener(this)
        btnHapusKat.setOnClickListener(this)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        id_kat = paket?.getString("id_kat").toString()
    }

    override fun onStart() {
        super.onStart()
        showDetailKat(id_kat)
    }

    fun showDetailKat(idk: String) {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                txIdKat.setText(jsonObject.getString("id_kat"))
                txNmKat.setText(jsonObject.getString("nm_kat"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_kat",idk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun hapusDataKat(idk: String) {
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil menghapus data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else{
                    Toast.makeText(this,"Gagal menghapus data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","delete")
                hm.put("id_kat",idk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
            hapusDataKat(id_kat)
        }
}