package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kelola_transaksi.*
import org.json.JSONArray

class TransaksiKasirActivity: AppCompatActivity(), View.OnClickListener {

    val F_TID = "id_trx"
    val F_TTGL = "tanggal"
    val F_TSTATUS = "status"
    val F_PNAMA = "nm_peg"
    val F_CNAMA = "customer"

    lateinit var alTrx : ArrayList<HashMap<String,Any>>
    lateinit var adapterTransaksi: AdapterTransaksi
    var url = "http://elvinatoko.000webhostapp.com/show_transaksi.php"
    var id_peg = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahTrx ->{
                val intent = Intent(this, TambahTransaksiActivity::class.java  )
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola_transaksi)
        alTrx = ArrayList() //new

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()

        btnTambahTrx.setOnClickListener(this)
        lsTrx.onItemClickListener = itemClick
    }

    override fun onStart() {
        super.onStart()
        showData()
    }

    fun showData(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                alTrx.clear()
                Log.d("isi","$response")
                if(response!=""){
                    val jsonArray = JSONArray(response)
                    for(x in 0 .. (jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        val hm = HashMap<String,Any>()
                        hm.put(F_TID, jsonObject.getString(F_TID))
                        hm.put(F_TTGL, jsonObject.getString(F_TTGL))
                        hm.put(F_PNAMA, jsonObject.getString(F_PNAMA))
                        hm.put(F_CNAMA, jsonObject.getString(F_CNAMA))
                        hm.put(F_TSTATUS, jsonObject.getString(F_TSTATUS))
                        alTrx.add(hm)
                    }
                    adapterTransaksi = AdapterTransaksi(this,alTrx)
                    lsTrx.adapter = adapterTransaksi
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_peg",id_peg)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alTrx.get(position)
        var pilihTrx = hm.get(F_TID).toString()
        val intent = Intent(this, DetailTrxActivity::class.java)
        intent.putExtra("id_peg",id_peg)
        intent.putExtra("id_trx",pilihTrx)
        intent.putExtra("kasir","kasir")
        startActivity(intent)
    }

}