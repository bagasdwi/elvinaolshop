package dwi.bagas.elvinaolshop

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AdapterDetailTransaksi (val context: Context,
                              arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter(){
    val F_TID = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_DTHARGA = "harga"
    val F_DTJML = "jumlah"
    val list = arrayList

    inner class ViewHolder(){
        var txID : TextView? = null
        var txNamaProduk : TextView? = null
        var txHargaProduk : TextView? = null
        var txJumlah : TextView? = null
    }
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_detail_trx, null, true)

            holder.txID = view!!.findViewById(R.id.txID) as TextView
            holder.txNamaProduk = view!!.findViewById(R.id.txNamaProduk) as TextView
            holder.txHargaProduk = view!!.findViewById(R.id.txHargaProduk) as TextView
            holder.txJumlah = view!!.findViewById(R.id.txJumlah) as TextView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        holder.txID!!.setText("Kode Barang : "+list.get(position).get(F_TID).toString())
        holder.txNamaProduk!!.setText("Produk : "+list.get(position).get(F_BNAMA).toString())
        var harga = formatRp(list.get(position).get(F_DTHARGA).toString())
        holder.txHargaProduk!!.setText("Harga satuan : Rp "+harga)
        holder.txJumlah!!.setText("Jumlah : "+list.get(position).get(F_DTJML).toString())

        return view!!
    }

    fun formatRp(angka:String): String? {
        val dec = DecimalFormat("#,###.##")
        return dec.format(angka.toInt())
    }
}
