package dwi.bagas.elvinaolshop


import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import kotlin.collections.HashMap

class RegisterActivity: AppCompatActivity(), View.OnClickListener {

    val mainUrl = "http://elvinatoko.000webhostapp.com/"
    val url = mainUrl+"autentikasi.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btnRegister.setOnClickListener(this)
    }

    fun queryInsert(mode : String){
    val request = object : StringRequest(
        Method.POST,url,
        Response.Listener { response ->
            Log.i("info","["+response+"]")
            val jsonObject = JSONObject(response)
            val error = jsonObject.getString("kode")
            if(error.equals("000")){
                Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                this.finish()
            }else{
                Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
            }
        },
        Response.ErrorListener { error ->
            Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
        }){
        override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            when(mode){
                "register" ->{
                    hm.put("mode","register")
                    hm.put("nm_peg",edRegNama.text.toString())
                    hm.put("username",edRegUserName.text.toString())
                    hm.put("password",edRegPassword.text.toString())
                }
            }
            return hm
        }
    }
    val queue = Volley.newRequestQueue(this)
    queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnRegister ->{
                queryInsert("register")
            }
        }
    }
}
