package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_tambah_trx.*
import org.json.JSONArray
import org.json.JSONObject

class EditTrxActivity: AppCompatActivity(), View.OnClickListener {

    val F_TID = "id_trx"
    val F_BID = "kd_barang"
    val F_BNAMA = "nm_barang"
    val F_DTHARGA = "harga"
    val F_DTJML = "jumlah"

    lateinit var alTrx : ArrayList<HashMap<String,Any>>
    lateinit var adapterDetail: AdapterDetailTransaksi
    var url = "http://elvinatoko.000webhostapp.com/show_detail_transaksi.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_transaksi.php"
    var url3 = "http://elvinatoko.000webhostapp.com/cek_transaksi.php"
    var id_peg = ""
    var id_trx = ""
    var pilihTrx = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLsBrng ->{
                val intent = Intent(this, TambahDBActivity::class.java)
                intent.putExtra("edit","edit")
                intent.putExtra("id_peg",id_peg)
                intent.putExtra("id_trx",id_trx)
                startActivity(intent)
            }
            R.id.btnTDetail ->{
                tambahDetail()
            }
            R.id.btnHDetail ->{
                hapusDetail()
            }
            R.id.btnProsesTrx ->{
                val intent = Intent(this, KonfirmEditTrxActivity::class.java)
                intent.putExtra("id_peg",id_peg)
                intent.putExtra("id_trx",id_trx)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_trx)
        alTrx = ArrayList() //new

        var paket: Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
        id_trx = paket?.getString("id_trx").toString()
        var kdb = paket?.getString("kd_barang").toString()
        if(kdb!="null"){
            edKdBrg.setText(kdb)
        }

        btnLsBrng.setOnClickListener(this)
        btnTDetail.setOnClickListener(this)
        btnHDetail.setOnClickListener(this)
        btnProsesTrx.setOnClickListener(this)
        lsDetailTrx.onItemClickListener = itemClick
    }

    override fun onStart() {
        super.onStart()
        showData()
    }

    fun showData(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.d("isi","id_trx =  $id_trx")
                alTrx.clear()
                Log.d("isi","apa nih $response")
                if (response!="") {
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length() - 1)) {
                        val jsonObject = jsonArray.getJSONObject(x)
                        val hm = HashMap<String, Any>()
                        hm.put(F_TID, jsonObject.getString(F_TID))
                        hm.put(F_BID, jsonObject.getString(F_BID))
                        hm.put(F_BNAMA, jsonObject.getString(F_BNAMA))
                        hm.put(F_DTHARGA, jsonObject.getString(F_DTHARGA))
                        hm.put(F_DTJML, jsonObject.getString(F_DTJML))
                        alTrx.add(hm)
                    }
                }
                adapterDetail = AdapterDetailTransaksi(this, alTrx)
                lsDetailTrx.adapter = adapterDetail
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_trx",id_trx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun tambahDetail(){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    showData()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","insertdetail")
                hm.put("id_trx",id_trx)
                hm.put("kd_barang",edKdBrg.text.toString())
                hm.put("harga",edHarga.text.toString())
                hm.put("jumlah",edJumlah.text.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun hapusDetail(){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")) {
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    showData()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","deletedetail")
                hm.put("id_trx",id_trx)
                hm.put("kd_barang",pilihTrx)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alTrx.get(position)
        pilihTrx = hm.get(F_BID).toString()
        edKdBrg.setText(pilihTrx)
        edHarga.setText("")
        edJumlah.setText("")
    }
}