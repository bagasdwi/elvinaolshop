package dwi.bagas.elvinaolshop

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class AdapterTransaksi(val context: Context,
                       arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter(){
    val F_TID = "id_trx"
    val F_TTGL = "tanggal"
    val F_TSTATUS = "status"
    val F_PNAMA = "nm_peg"
    val F_CNAMA = "customer"
    val list = arrayList

    inner class ViewHolder(){
        var txID : TextView? = null
        var txTgl : TextView? = null
        var txPeg : TextView? = null
        var txCust : TextView? = null
        var txStatus : TextView? = null
    }
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_trx, null, true)

            holder.txID = view!!.findViewById(R.id.trxID) as TextView
            holder.txTgl = view!!.findViewById(R.id.trxTgl) as TextView
            holder.txPeg = view!!.findViewById(R.id.trxPeg) as TextView
            holder.txCust = view!!.findViewById(R.id.trxCust) as TextView
            holder.txStatus = view!!.findViewById(R.id.trxStatus) as TextView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        holder.txID!!.setText("ID : "+list.get(position).get(F_TID).toString())
        holder.txTgl!!.setText("Tanggal : "+list.get(position).get(F_TTGL).toString())
        holder.txPeg!!.setText("Pegawai : "+list.get(position).get(F_PNAMA).toString())
        holder.txCust!!.setText("Customer : "+list.get(position).get(F_CNAMA).toString())
        holder.txStatus!!.setText("Status : "+list.get(position).get(F_TSTATUS).toString())

        return view!!
    }
}
