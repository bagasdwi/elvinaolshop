package dwi.bagas.elvinaolshop

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val mainUrl = "http://elvinatoko.000webhostapp.com/"
    val url = mainUrl+"autentikasi.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }

    fun login(){
        val request = object : StringRequest(
            Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("222")){
                    var id = jsonObject.getString("id_peg").toString()
                    var uname = jsonObject.getString("username").toString()
                    var level = jsonObject.getString("level").toString()
                    Toast.makeText(this,"Selamat datang "+uname, Toast.LENGTH_LONG).show()
                    if(level=="admin"){
                        val intent = Intent(this, AdminActivity::class.java  )
                        intent.putExtra("id_peg",id)
                        startActivity(intent)
                    }else{
                        val intent = Intent(this, HalamanPegawaiActivity::class.java  )
                        intent.putExtra("id_peg",id)
                        startActivity(intent)
                    }


                }else if(error.equals("333")){
                    Toast.makeText(this,"Password yang anda masukkan salah", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Username tidak ditemukan", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()

                hm.put("mode","login")
                hm.put("username",edUserName.text.toString())
                hm.put("password",edPassword.text.toString())


                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btnLogin ->{
                login()
            }
            R.id.txSignUp ->{
                val intent = Intent(this, RegisterActivity::class.java  )
                startActivity(intent)
            }
        }
    }
}
