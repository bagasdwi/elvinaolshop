package dwi.bagas.elvinaolshop

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kelola_barang.*
import org.json.JSONArray

class BarangActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var barangAdapter : AdapterBarang
    var daftarBarang = mutableListOf<HashMap<String,String>>()
    var url = "http://elvinatoko.000webhostapp.com/show_barang.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahBrg ->{
                val intent = Intent(this, TambahBrgActivity::class.java  )
                startActivity(intent)
            }
            R.id.btnCariBrg ->{
                showBarang(edSearchBrg.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola_barang)

        barangAdapter = AdapterBarang(daftarBarang,this) //new
        lsBrg.layoutManager = LinearLayoutManager(this)
        lsBrg.adapter = barangAdapter

        btnCariBrg.setOnClickListener(this)
        btnTambahBrg.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showBarang("")
    }

    fun showBarang(brg: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarBarang.clear()
                Log.d("barang","$response")
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var barang = HashMap<String,String>()
                    barang.put("kd_barang",jsonObject.getString("kd_barang"))
                    barang.put("nm_barang",jsonObject.getString("nm_barang"))
                    barang.put("stok",jsonObject.getString("stok"))
                    barang.put("nm_kat",jsonObject.getString("nm_kat"))
                    barang.put("url",jsonObject.getString("url"))
                    daftarBarang.add(barang)
                }
                barangAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nm_barang",brg)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDetail(kdb : String){
        val intent = Intent(this, DetailBrgActivity::class.java  )
        intent.putExtra("kd_barang",kdb)
        startActivity(intent)
    }
}