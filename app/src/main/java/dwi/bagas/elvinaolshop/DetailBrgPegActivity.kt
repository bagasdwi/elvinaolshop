package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_brg_admin.*
import org.json.JSONObject

class DetailBrgPegActivity: AppCompatActivity() {

    //Inisialisasi Variabel
    lateinit var dialog: AlertDialog.Builder
    var kd_barang: String = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_barang.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_barang.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_brg_pegawai)

        dialog = AlertDialog.Builder(this)
        var paket: Bundle? = intent.extras
        kd_barang = paket?.getString("kd_barang").toString()
    }

    override fun onStart() {
        super.onStart()
        showDetailBarang(kd_barang)
    }

    fun showDetailBarang(kdb: String) {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                txKdBrgD.setText(kdb)
                txNmBrgD.setText(jsonObject.getString("nm_barang"))
                txKategoriD.setText(jsonObject.getString("nm_kat"))
                Picasso.get().load(jsonObject.getString("url")).into(imBrgD);
                txStokD.setText(jsonObject.getString("stok"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("kd_barang",kdb)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        hapusDataBarang(kd_barang)
    }

    fun hapusDataBarang(kdb: String) {
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil menghapus data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else{
                    Toast.makeText(this,"Gagal menghapus data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","delete")
                hm.put("kd_barang",kdb)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}