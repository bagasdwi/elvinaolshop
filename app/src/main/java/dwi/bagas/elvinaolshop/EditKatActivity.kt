package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_edit_kategori.*
import org.json.JSONObject

class EditKatActivity: AppCompatActivity(), View.OnClickListener {

    //Inisialisasi Variabel
    lateinit var dialog: AlertDialog.Builder
    var id_kat: String = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_kategori.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_kategori.php"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnEditKatE -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_kategori)

        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        id_kat = paket?.getString("id_kat").toString()
        btnEditKatE.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDetailKat(id_kat)
    }

    fun showDetailKat(idk: String) {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                edNmKatE.setText(jsonObject.getString("nm_kat"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_kat",idk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataKat(id_kat)
    }

    fun updateDataKat(idk: String) {
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil mengupdate data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else{
                    Toast.makeText(this,"Gagal mengupdate data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","update")
                hm.put("id_kat",id_kat)
                hm.put("nm_kat",edNmKatE.text.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}