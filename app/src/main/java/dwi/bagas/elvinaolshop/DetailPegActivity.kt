package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_detail_pegawai_admin.*
import org.json.JSONObject

class DetailPegActivity: AppCompatActivity(), View.OnClickListener {

    //Inisialisasi Variabel
    lateinit var dialog: AlertDialog.Builder
    var id_peg: String = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_pegawai.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_pegawai.php"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnEditPeg -> {
                val intent = Intent(this, EditPegActivity::class.java  )
                intent.putExtra("id_peg",id_peg)
                startActivity(intent)
            }
            R.id.btnHapusPeg -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pegawai_admin)

        btnEditPeg.setOnClickListener(this)
        btnHapusPeg.setOnClickListener(this)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
    }

    override fun onStart() {
        super.onStart()
        showDetailPeg(id_peg)
    }

    fun showDetailPeg(idp: String) {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                txIdPeg.setText(jsonObject.getString("id_peg"))
                txNmPeg.setText(jsonObject.getString("nm_peg"))
                txLevelD.setText(jsonObject.getString("level"))
                txUsernameD.setText(jsonObject.getString("username"))
                txPasswordD.setText(jsonObject.getString("password"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_peg",idp)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun hapusDataKat(idp: String) {
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil menghapus data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else{
                    Toast.makeText(this,"Gagal menghapus data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","delete")
                hm.put("id_peg",idp)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        hapusDataKat(id_peg)
    }
}