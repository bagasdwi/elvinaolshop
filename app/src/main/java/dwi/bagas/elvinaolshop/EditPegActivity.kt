package dwi.bagas.elvinaolshop

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_edit_pegawai.*
import org.json.JSONObject

class EditPegActivity: AppCompatActivity(), View.OnClickListener {

    //Inisialisasi Variabel
    lateinit var dialog: AlertDialog.Builder
    var id_peg: String = ""
    var url = "http://elvinatoko.000webhostapp.com/show_detail_pegawai.php"
    var url2 = "http://elvinatoko.000webhostapp.com/query_crud_pegawai.php"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnEditPegE -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_pegawai)

        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        id_peg = paket?.getString("id_peg").toString()
        btnEditPegE.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDetailPeg(id_peg)
    }

    fun showDetailPeg(idp: String) {
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                edNmPegE.setText(jsonObject.getString("nm_peg"))
                edPasswordE.setText(jsonObject.getString("password"))
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_peg",idp)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataPeg(id_peg)
    }

    fun updateDataPeg(idp: String) {
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Berhasil mengupdate data", Toast.LENGTH_LONG).show()
                    this.finish()
                }else{
                    Toast.makeText(this,"Gagal mengupdate data", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("mode","update")
                hm.put("id_peg",id_peg)
                hm.put("nm_peg",edNmPegE.text.toString())
                hm.put("password",edPasswordE.text.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}